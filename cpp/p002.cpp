//	30/09/2021

//	projecteuler problem 2
//	"Even Fibonacci numbers"

#include <iostream>

using namespace std;

int main()
{
	int a{ 1 };

	int old{ 1 };

	int total{ 0 };

	for (int next = 1; next < 4000000; )
	{
		old = next;
		next += a;
		a = old;

		cout << next << " "<< endl;

		if (!(next % 2))
		{
			cout << "even number being added: " << next << endl;
			total += next;
		}
	}
	cout << "total sum of even numbers = " << total;

	return 0;
}