/**
*	30/10/2022
*
*	projecteuler problem 19 - Counting Sundays
*
*	You are given the following information,
*	but you may prefer to do some research for yourself.
*
*	1 Jan 1900 was a Monday.
*	Thirty days has September, April, June and November.
*	All the rest have thirty-one,
*	Saving February alone,
*	Which has twenty-eight, rain or shine.
*	And on leap years, twenty-nine.
*	A leap year occurs on any year evenly divisible by 4,
*	but not on a century unless it is divisible by 400.
*	How many Sundays fell on the first of the month
*	during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
*/

#include <stdio.h>

int iMonthDays[12] =
{
	31,	//	January
	28,	//	February
	31,	//	March
	30,	//	April
	31,	//	May
	30,	//	June
	31,	//	July
	31,	//	August
	30,	//	September
	31,	//	October
	30,	//	November
	31	//	December
};

//	Non-Leap year = 365 days
//		Leap year = 366 days

int IsLeapYear(int* year)
{
	if (!(*year % 400)) return 2;	//	Century Leap Year
	if (!(*year % 4)) return 1;		//	Leap Year

	return 0;
}

int main()
{
//	int _year = 1600;
//	printf("Is %i a leap year? R: %s", _year, (IsLeapYear(&_year) ? "Yes" : "No"));
	
	int OnLeapYear = 0;
	int Days = 0;
	int iMaxDaysOfTheYear = 0;
	int TargetSundays = 0;

	for (int year = 1900; year <= 2000; ++year)
	{
		if(Days)
		{
			Days -= iMaxDaysOfTheYear;
		}

		OnLeapYear = IsLeapYear(&year);

		if (OnLeapYear)
		{
			iMaxDaysOfTheYear = 366;
		}
		else
		{
			iMaxDaysOfTheYear = 365;
		}

		int CurrentMonth = 0;
		int TargetDays = iMonthDays[CurrentMonth];

	//	printf("Year: %i - Is leap year: %s\n", year, OnLeapYear ? "Yes" : "No");

		while (Days <= iMaxDaysOfTheYear)
		{			
			Days += 7;

			if (Days >= TargetDays)
			{
				if (Days == TargetDays && year != 1900)
				{
					++TargetSundays;
				}
				++CurrentMonth;
				if (CurrentMonth == 1 && OnLeapYear)
				{
					TargetDays += 29;
				}
				else
				{
					TargetDays += iMonthDays[CurrentMonth];
				}
			}
		}

	}
	printf("How many Sundays that fell on the 1st of the month from 1991 to 2000:\n=> %i", TargetSundays);

	
	return 0;
}
