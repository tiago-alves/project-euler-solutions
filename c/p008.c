/**
*	24/09/2022
*
*	projecteuler problem 8 - Largest product in a series
*
*	The four adjacent digits in the 1000-digit number that have
*	the greatest product are 9 × 9 × 8 × 9 = 5832.
*
* 	73167176531330624919225119674426574742355349194934
*	96983520312774506326239578318016984801869478851843
*	85861560789112949495459501737958331952853208805511
*	12540698747158523863050715693290963295227443043557
*	66896648950445244523161731856403098711121722383113
*	62229893423380308135336276614282806444486645238749
*	30358907296290491560440772390713810515859307960866
*	70172427121883998797908792274921901699720888093776
*	65727333001053367881220235421809751254540594752243
*	52584907711670556013604839586446706324415722155397
*	53697817977846174064955149290862569321978468622482
*	83972241375657056057490261407972968652414535100474
*	82166370484403199890008895243450658541227588666881
*	16427171479924442928230863465674813919123162824586
*	17866458359124566529476545682848912883142607690042
*	24219022671055626321111109370544217506941658960408
*	07198403850962455444362981230987879927244284909188
*	84580156166097919133875499200524063689912560717606
*	05886116467109405077541002256983155200055935729725
*	71636269561882670428252483600823257530420752963450
*
*	Find the thirteen adjacent digits in the 1000-digit
*	number that have the greatest product.
*	What is the value of this product?
*
*/

#include <stdio.h>

#define MAX_NUMBER_DIGITS     1000      	//  50  Numbers + 1 Null Char   (0-49)

char szString[MAX_NUMBER_DIGITS + 1] =      //  MAX_NUMBER_DIGITS CHARS + NULL CHAR
"73167176531330624919225119674426574742355349194934\
96983520312774506326239578318016984801869478851843\
85861560789112949495459501737958331952853208805511\
12540698747158523863050715693290963295227443043557\
66896648950445244523161731856403098711121722383113\
62229893423380308135336276614282806444486645238749\
30358907296290491560440772390713810515859307960866\
70172427121883998797908792274921901699720888093776\
65727333001053367881220235421809751254540594752243\
52584907711670556013604839586446706324415722155397\
53697817977846174064955149290862569321978468622482\
83972241375657056057490261407972968652414535100474\
82166370484403199890008895243450658541227588666881\
16427171479924442928230863465674813919123162824586\
17866458359124566529476545682848912883142607690042\
24219022671055626321111109370544217506941658960408\
07198403850962455444362981230987879927244284909188\
84580156166097919133875499200524063689912560717606\
05886116467109405077541002256983155200055935729725\
71636269561882670428252483600823257530420752963450";

int main()
{
    unsigned long long int greatestProduct = 0;

    for (unsigned short i = 0; i <= (MAX_NUMBER_DIGITS - 13); ++i)
    {
        //  <= (MAX_NUMBER_DIGITS - 13) :   No point in looking if the remaining digits < 13

    /*  unsigned short iLineStart = (i / 50)*50;

        if (i > iLineStart + 37 && i < iLineStart + 50)
        {
            i = iLineStart + 50;
        }   */

        char substring[14];

        char* s = &szString[i];
        strncpy_s(substring, 14, s, 13);

        printf("%04hu: %s\n", i, substring);

        s = strchr(substring, 0x30);  //  0x30 or '0'

        if (s != NULL)
        {
            printf("Found a zero\n");
        }
        else
        {
            unsigned long long int product = 1;
            for (unsigned int k = 0; k < strlen(substring); ++k)
            {
                unsigned int integer = substring[k] - '0';
                product *= integer;

                printf("%u (char %c) multiplied to product = %llu\n", integer, substring[k], product);

                if (k == strlen(substring) - 1)
                {
                    printf("= %llu\n", product);
                }
            }
            if (product > greatestProduct)
            {
                //  is the product of these 13 digits greater than the previous one? Keep it
                greatestProduct = product;
            }
        }
    }

    printf("greatest product is %llu\n", greatestProduct);

    return 0;
}

/*
*   references
*       efficiency of C Strings vs C++ Strings
*           https://stackoverflow.com/questions/12124263/efficiency-of-c-string-vs-cstrings
*       small trick to convert ASCII number chars into actual integers
*           https://scholarsoul.com/convert-char-to-int-in-c/
*       printf reference
*           https://cplusplus.com/reference/cstdio/printf/
*       printing constants that are not integers
*           https://stackoverflow.com/questions/1385818/c-printing-big-numbers
*       curiosity: unsigned performance
*           https://stackoverflow.com/questions/4712315/performance-of-unsigned-vs-signed-integers
*/