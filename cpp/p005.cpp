/**
*	20/09/2022
*
*	projecteuler problem 5 - Smallest multiple
*
*	2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
*	What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*
*/

#include <iostream>

int main()
{
	for (unsigned int i = 1; ; i++)
	{
	//	std::cout << "i: " << i << std::endl;	//	should probably not output text, 
												//	otherwise it takes way longer to find the number
		unsigned short n = 1;
		while (!(i % n))
		{
		//	printf("n: %i\n", n);				//	ditto
			if (n == 20) break;
			else ++n;
		}
		if (n == 20)
		{
			printf("answer is %i", i);
			break;
		}
	}
	return 0;
}