/**
*	03/11/2022
*
*	projecteuler problem 20 - Factorial digit sum
*
*	n! means n × (n − 1) × ... × 3 × 2 × 1
*
*	For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
*	and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
*
*	Find the sum of the digits in the number 100!
*/

#include <stdio.h>
#include <string.h>

#define debug(...)	//printf(__VA_ARGS__)

void StrRevert(char* _str)	//	turns a string backwards
{
	char temp;
	int _length = strlen(_str);
//	debug("strRevert(%s)\nLength: %i\n", _str, _length);

	for (int k = 0; k < _length / 2; ++k)
	{
		temp = _str[k];
		_str[k] = _str[_length - (k + 1)];
		_str[_length - 1 - k] = temp;
	}
}

void AddZero(char* _str, int amount)
{
	for (int i = strlen(_str); i >= 0; --i)
	{
		_str[i + amount] = _str[i];
	}
	for (int i = 0; i < amount; ++i)
	{
		_str[i] = '0';
	}
}

void StrAdd(char* FirstTerm, char* SecondTerm)	//StrAdd "dest" "src"
{
	int iLengthDiff = (strlen(SecondTerm) - strlen(FirstTerm));

	debug("length difference: %i\n", iLengthDiff);

	if (!strlen(FirstTerm))	//	length difference changed sign == strlen
	{
		debug("%s + %s = \n", FirstTerm, SecondTerm);
		strcpy_s(FirstTerm, iLengthDiff + 1, SecondTerm);
		return;
	}

//	StrRevert(FirstTerm);
//	StrRevert(SecondTerm);

	int LongestLength;

	if (iLengthDiff > 0)
	{
		LongestLength = strlen(FirstTerm);
	//	char padding[20];
		for (int a = 0; a < iLengthDiff; ++a)
		{
		//	padding[a] = '0';
			FirstTerm[LongestLength + a] = '0';
		}
		FirstTerm [LongestLength + iLengthDiff] = '\0';

	//	sprintf_s(FirstTerm, sizeof(FirstTerm) , "%s%s", FirstTerm, padding);

		debug("After padding: \"%s\"\n", FirstTerm);
		LongestLength = strlen(SecondTerm);
	}
	else
	{
		LongestLength = strlen(FirstTerm);
	}

	int SecondTermLength = strlen(SecondTerm);
	debug("Longest String: %i\n", LongestLength);
	int remainer = 0;

		debug("FirstTerm Length: %i\n", iLengthDiff);
	for (int i = 0; i < LongestLength; ++i)
	{
		int n = FirstTerm[i] - '0';
		int	j;
		if (i < SecondTermLength && SecondTerm[i] >= 0x30 && SecondTerm[i] <= 0x39)
		{
			j = SecondTerm[i] - '0';
		}
		else
		{
			j = 0;
		}
		debug("%i + %i + %i = ", n, j, remainer);
		n += j + remainer;
		debug("%i\n", n);

		remainer = 0;

		if (n > 9)
		{
			debug("if (n > 9): %i\n", n);
			n -= 10;
			debug("n -= 10 == %i\n", n);
			remainer = 1;
		}

		FirstTerm[i] = n + '0';
		debug("replacing char FirstTerm[%i]: %i (char: %c)\n", i, n, FirstTerm[i]);

		if (i == LongestLength - 1 && remainer > 0)
		{
			debug("i: %i, remainer: %i\n", i, remainer);
			remainer += '0';
			sprintf_s(FirstTerm, 256, "%s%c", FirstTerm, remainer);
			debug("after adding remainer: %s\n", FirstTerm);
		}
	}
//	StrRevert(FirstTerm);
//	StrRevert(SecondTerm);

		debug("%s + %s = \n", FirstTerm, SecondTerm);
		debug("= %s\n", FirstTerm);
}

char BigNumber[256];

int main()
{
	int number = 100;
	sprintf_s(BigNumber,sizeof(BigNumber), "%i", number);

	StrRevert(BigNumber);
	int len = strlen(BigNumber);


	debug("BigNumber: %s | Length: %i\n", BigNumber, len);

	for (int i = number-1; i > 0; --i)
	{
		debug("\n");
		char TempDest[256];
		char TempSrc[256];

		char multiply[5];
		sprintf(multiply, "%i", i);
		StrRevert(multiply);
		int multiplyLen = strlen(multiply);
		debug("multiply: %s | Length: %i\n", multiply, multiplyLen);

		for (int mul = 0; mul < multiplyLen; ++mul)
		{
			int carry = 0;
			if (!mul)
			{
				memset(TempDest, 0, sizeof(TempDest));
			//	sprintf_s(TempDest, sizeof(TempDest), "");
			}
			memset(TempSrc, 0, sizeof(TempSrc));
		//	sprintf_s(TempSrc, sizeof(TempSrc), "");

			for (int iDigitPos = 0; iDigitPos < len; ++iDigitPos)
			{				
				int n = BigNumber[iDigitPos] - '0';

				debug("BigNumber[%i] = %c | n = %i\n", iDigitPos, BigNumber[iDigitPos], n);
				n *= (multiply[mul] - '0');
				debug("n * multiply[%i] == n * %c = %i\n", mul, multiply[mul], n);
				n += carry;

				carry = 0;

				if (n > 9)
				{
					carry = n / 10;
					n %= 10;
				}

				TempSrc[iDigitPos] = n + '0';

				TempSrc[iDigitPos + 1] = '\0';

			/*	debug("TempSrc: ");
				for (int g = iDigitPos; g >= 0; --g)
				{
					debug("%c", TempSrc[g]);
				}
				debug("\n");*/

				if (iDigitPos == (len - 1) )
				{
					if (carry > 0)
					{
						++iDigitPos;
						n = carry;
						TempSrc[iDigitPos] = n + '0';
					}

				//	n = BigNumber[iDigitPos] - '0';
					TempSrc[iDigitPos+1] = '\0';

				//	debug("k + 1 = %i, %i %i\n", k, BigNumber[k]);

				//	TempSrc[iDigitPos+1] = '\n';
					debug("len now is = %i\n", len);
					/*	for (int g = len - 1; g >= 0; --g)
						{
							debug("%i", BigNumber[g]);
						}*/
					debug("\n");		

					if (mul == 0)
					{
					/*	debug("TempSrc: ");
						for (int g = iDigitPos; g >= 0; --g)
						{
							debug("%c", TempSrc[g]);
						}	*/
						strcpy_s(TempDest, sizeof(TempDest), TempSrc);
					/*	debug(" == TempDest: ");
						for (int g = iDigitPos; g >= 0; --g)
						{
							debug("%c", TempDest[g]);
						}

						debug("\n");	*/
					}
					else
					{
						AddZero(TempSrc, mul);

						debug("TempDest: %s | TempSrc: %s\n", TempDest, TempSrc);

						StrAdd(TempDest, TempSrc);
					}
				}
			}
		}

		strcpy_s(BigNumber, sizeof(BigNumber), TempDest);
		debug("New BigNumber: ");
		len = strlen(BigNumber);
		for (int g = len; g >= 0; --g)
		{
			debug("%c", BigNumber[g]);
		}
		debug("\n");
		debug("----------------------------------------------\n");
	}

	printf("Printing BigNumber: ");

	int total = 0;
	for (int g = len - 1; g >= 0; --g)
	{
		printf("%c", BigNumber[g]);
		total += (BigNumber[g] - '0');
	}
	printf("\n");
	printf("Total: %i", total);

	return 0;
}
