/**
*	03/09/2022
*
*	projecteuler problem 15 - Lattice paths
*
*	Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down,
*	there are exactly 6 routes to the bottom right corner.
*		 _______
*		|	|	|
*		|___|___|
*		|	|	|
*		|___|___|
**
*	How many such routes are there through a 20×20 grid?
*/

#include <stdio.h>

#define GRID		20
#define MAX_GRID	GRID+1

unsigned long long grid[MAX_GRID][MAX_GRID];

int main()
{
	for (unsigned long long line = 0; line < MAX_GRID; ++line)
	{
		printf("| ");
		for (unsigned long long column = 0; column < MAX_GRID; ++column)
		{
			if (line == 0)
			{
				grid[line][column] = 1;
				printf("%-12llu ", grid[line][column]);
				if (column == MAX_GRID - 1)
				{
					printf(" |\n");
				}
				continue;
			}
			if (column == 0)
			{
				grid[line][column] = 1;
				printf("%-12llu ", grid[line][column]);
				continue;
			}
			grid[line][column] = grid[line - 1][column] + grid[line][column - 1];

			printf("%-12llu ", grid[line][column]);

			if (column == MAX_GRID - 1)
			{
				printf(" |\n");
			}
		}
	}

	printf("result: %llu", grid[MAX_GRID - 1][MAX_GRID - 1]);

	return 0;
}
