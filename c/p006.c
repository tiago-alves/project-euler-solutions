/**
*	20/09/2022
*
*	projecteuler problem 6 - Sum square difference
*
*	The sum of the squares of the first ten natural numbers is,
*
*		1^2 + 2^2 + 3^2 + ... + 10^2 = 385
*
*	The square of the sum of the first ten natural numbers is,
*
*		(1 + 2 + 3 + ... + 10)^2 = 55^2 = 3025
*
*	Hence the difference between the sum of the squares of
*	the first ten natural numbers and the square of the sum is
*
*		3025 - 385 = 2640
*
*	Find the difference between the sum of the squares of
*	the first one hundred natural numbers and the square of the sum.
*
*/

#include <stdio.h>

int main()
{
	unsigned int iSumOfSquares= 0;
	unsigned int iSquareOfSums= 0;

	int iTotal= 0;

	for (unsigned short i = 1; i <= 100; ++i)
	{
		iSumOfSquares += (i * i);
		iSquareOfSums += i;

		printf("i: %hu | Sum Of Squares: %u | Square Of Sums: %u\n", i, iSumOfSquares, iSquareOfSums);

		if (i == 100)
		{
			iSquareOfSums *= iSquareOfSums;

			iTotal = (iSquareOfSums - iSumOfSquares);

			printf("Answer: %i", iTotal);
		}
	}
	return 0;
}