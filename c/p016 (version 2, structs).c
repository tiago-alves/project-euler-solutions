/**
*	13/10/2022
*
*	projecteuler problem 16 - Power digit sum
*
*	2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
*
*	What is the sum of the digits of the number 2^1000 ?
*/

#include <stdio.h>


struct HugeInt_s 
{
	int Int[512];
	int Len;
	int Positive;
} HugeInt_default = { {0}, 1, 1 };

typedef struct HugeInt_s HugeInt;


void HugeInt_Print(HugeInt P)
{
	for (int g = P.Len-1; g >= 0; --g)
	{
		printf("%i", P.Int[g]);
	}
	printf("\n");
}

int HugeInt_Length(HugeInt L)
{
	return (int)L.Len;
}

int HugeInt_ReturnDigit(HugeInt* Num, int digit)
{
	return Num->Int[digit];
}

void HugeInt_RaiseToPow(HugeInt* Num, int Base, int Pow)
{
	Num->Int[0] = 1;

	for (int i = 1; i <= Pow; ++i)
	{
		int carry = 0;

		for (int k = 0; k < Num->Len; ++k)
		{
			Num->Int[k] *= Base;
			Num->Int[k] += carry;
			carry = 0;
			if (Num->Int[k] > 9)
			{
				carry = Num->Int[k] / 10;
				Num->Int[k] %= 10;
			}

			if (k == (Num->Len - 1) && carry > 0)
			{
				k++;
				Num->Int[k] += carry;
				++Num->Len;
				break;
			}
		}
	}
}

int main()
{
	HugeInt Number = HugeInt_default;

//	Default values

	HugeInt_Print(Number);
	printf("Number Length = %i\n", HugeInt_Length(Number));

	printf("--------------------------\n");

	HugeInt_RaiseToPow(&Number, 2, 1000);

//	Raised 2 to 1000

	HugeInt_Print(Number);
	printf("Number Length = %i\n", HugeInt_Length(Number));

//	Adding every digits

	int Total = 0;

	for (int i = HugeInt_Length(Number); i >= 0; --i)
	{
		Total += HugeInt_ReturnDigit(&Number, i);
	}

	printf("Sum of all digits: %i", Total);

	return 0;
}
