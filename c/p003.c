//	26/09/2021

//	project euler problem 3
//	"Largest prime factor"

/*
	The prime factors of 13195 are 5, 7, 13 and 29.

	What is the largest prime factor of the number 600851475143 ?
*/

#include <stdio.h>

int main()
{
	long long a = 600851475143;
	int prime = 2;

	while (a != 1)
	{
		//	printf("%i ", prime);

		if (!(a % prime)) 	// Chech if looping number is dividable (has 0 remainder)
		{
			printf("%lli / %i = ",a, prime);

			a = a / prime;	// 

			printf("%ll\n", a);
		}
		else
		{
			++prime;
		}
	}
	printf("found it: %i", prime);
	return 0;
}

