/**
*	13/10/2022
*
*	projecteuler problem 16 - Power digit sum
*
*	2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
*
*	What is the sum of the digits of the number 2^1000 ?
*/

#include <stdio.h>

int main()
{
	int BigNumber[512] = { 0 };

	int len = 1;

	BigNumber[0] = 1;

	for (int i = 1; i <= 1000; ++i)
	{
		int carry = 0;
	//	printf("Exponentialise for the %i time\n", i);
		for (int k = 0; k < len; ++k)
		{
			BigNumber[k] *= 2;
			BigNumber[k] += carry;
			carry = 0;
			if (BigNumber[k] > 9)
			{
				carry = BigNumber[k] / 10;
				BigNumber[k] %= 10;
			}

		//	printf("k = %i, len = %i, BigNumber[k] = %i (carrying %i)\n", k, len, BigNumber[k], carry);

			if (k == (len - 1) && carry > 0)
			{
				k++;
				BigNumber[k] += carry;
			//	printf("k + 1 = %i, %i %i\n", k, BigNumber[k]);
				++len;
			//	printf("len now is = %i\n", len);
			/*	for (int g = len - 1; g >= 0; --g)
				{
					printf("%i", BigNumber[g]);
				}*/
			//	printf("\n");
				break;
			}
			else
			{
			/*	for (int g = len - 1; g >= 0; --g)
				{
					printf("%i", BigNumber[g]);
				}
				printf("\n");*/
			}
		}
	}

	unsigned long total = 0;
	for (int g = len - 1; g >= 0; --g)
	{
		printf("%i", BigNumber[g]);
		total += BigNumber[g];
	}

	printf("\n");
	printf("Total: %i", total);

	return 0;
}
