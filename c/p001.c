//	26/09/2021

//	projecteuler problem 1
//	"Multiples of 3 or 5"

#include <stdio.h>

int main()
{
	int iSumOfMultiples = 0;

	for (short i = 1; i < 1000; ++i)
	{
		if (!(i % 3) || !(i % 5))
		{
			iSumOfMultiples += i;
		}
	}

	printf("sum of all multiples of 3 or 5 = %i\n", iSumOfMultiples);

	return 0;
}
