/**
*	26/09/2022
*
*	projecteuler problem 10 - Summation of primes
*
*   The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
*
*   Find the sum of all the primes below two million.
*/

#include <stdio.h>

int main()
{
	long long iSum = 0;
	int iCurrentPrime = 2;

	while (1)
	{
		for (int div = 2; iCurrentPrime > div; ++div)
		{
			if (!(iCurrentPrime % div))
			{
				++iCurrentPrime;
				div = 2;
			}
		}
		if (iCurrentPrime >= 2000000) break;

		iSum += iCurrentPrime;

		if (iCurrentPrime > 1990000)
		{
			printf("Prime number %07i added: %lli\n", iCurrentPrime, iSum);
		}
		++iCurrentPrime;
	}
	printf("Total sum: %lli\n", iSum);
	return 0;
}
