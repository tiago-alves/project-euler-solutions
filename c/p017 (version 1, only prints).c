/**
*	21/10/2022
*
*	projecteuler problem 17 - Number letter counts
*
*	If the numbers 1 to 5 are written out in words: one, two, three, four, five, 
*	then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
*
*	If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, 
*	how many letters would be used?
*
*	NOTE: Do not count spaces or hyphens. 
*	For example, 342 (three hundred and forty-two) contains 23 letters and 
*	115 (one hundred and fifteen) contains 20 letters. 
*	The use of "and" when writing out numbers is in compliance with British usage.
*/

#include <stdio.h>
#include <math.h>
#include <time.h>

//#define ONE_IS_A

/*	
	Hundreds
	Tens
	Units
*/

const char* Unit[10][6] =
{
	{ "" },
	{ "one" },
	{ "two" },
	{ "three" },
	{ "four" },
	{ "five" },
	{ "six" },
	{ "seven" },
	{ "eight" },
	{ "nine" }
};

const char* Tens[10][8] =
{
	{ "" },
	{ "ten" },
	{ "twenty" },
	{ "thirty" },
	{ "forty" },
	{ "fifty" },
	{ "sixty" },
	{ "seventy" },
	{ "eighty" },
	{ "ninety" }
};

const char* Teens[10][10] =
{
	{ "ten" },
	{ "eleven" },
	{ "twelve" },
	{ "thirteen" },
	{ "fourteen" },
	{ "fifteen" },
	{ "sixteen" },
	{ "seventeen" },
	{ "eighteen" },
	{ "nineteen" }
};

int main()
{
	clock_t begin = clock();

	int totalCharCount = 0;
	double dLogTen = 0.0;

	for (int iCurrentNum = 1; iCurrentNum <= 1000; ++iCurrentNum)
	{
		dLogTen = (double)iCurrentNum;
		dLogTen = log10(dLogTen);

		int iCurrentDigit = (int)dLogTen;

		int iExcess = 0;
	//	int iTotalDigits = iCurrentDigit;

		//	printf("> iCurrentNum = %i\n", n);

		int SkipTens = -2;

		while (iCurrentDigit >= 0)
		{
			int iDivider = (int)pow(10.0, (int)iCurrentDigit);

			int iIndex = iCurrentNum;
		/*	printf("Number %i, log10 %f (int)%i, Pow(%i), iExcess: %i\n", iIndex,
																			dLogTen, 
																			iCurrentDigit,
																			iDivider,
																			iExcess);	*/
			iIndex -= iExcess;
			iIndex /= iDivider;

		//	printf("=> iIndex = %i, k = %i, iExcess = %i\n", iIndex, k, iExcess);

			switch (iCurrentDigit)
			{
				case 3:
				//	printf("==> case 3: %s thousand\n", *Unit[iIndex]);
					if (iIndex != 0)
					{
#ifdef ONE_IS_A
						if (iIndex == 1)
						{
							printf("a thousand ");
						}
						else
						{
							printf("%s thousand ", *Unit[iIndex]);
						}
#else // ONE_IS_A
						printf("%s thousand ", *Unit[iIndex]);
#endif
					}
					break;
				case 2:
				//	printf("==> case 2: %s hundred\n", *Unit[iIndex]);
					if (iIndex != 0)
					{
#ifdef ONE_IS_A
						if (iIndex == 1)
						{
							printf("a hundred ");
						}
						else
						{
							printf("%s hundred ", *Unit[iIndex]);
						}
#else
						printf("%s hundred ", *Unit[iIndex]);
#endif
					}
					break;
				case 1:
				//	printf("==> case 1: %s\n", *Unit[iIndex]);
					if (iIndex == 0 || iIndex == 1)
					{
						SkipTens = iIndex;
						break;
					}
					else
					{
						SkipTens = -1;
						if(iExcess == 0)
							printf("%s", *Tens[iIndex]);
						else
							printf("and %s", *Tens[iIndex]);
					}
					break;
				case 0:
				//	printf("==> case 0: %s\n", *Unit[iIndex]);
					if (SkipTens == 0)
					{
						if (iIndex != 0)
						{
							if (iExcess == 0)
							{
								printf("%s", *Unit[iIndex]);
								break;
							}
							else
							printf("and %s", *Unit[iIndex]);
						}
						break;
					}
					else if (SkipTens == 1)
					{
						if (iCurrentNum < 20)
							printf("%s", *Teens[iIndex]);
						else
							printf("and %s", *Teens[iIndex]);
					}
					else if(SkipTens == -1)
					{
						if (iIndex != 0)
						{
							printf("-%s", *Unit[iIndex]);
						}
					}
					else
					{
						if (iIndex != 0)
						{
							printf("%s", *Unit[iIndex]);
						}
					}
					break;
			}
			iExcess += (iIndex * iDivider);
			--iCurrentDigit;
		}
		printf("\n");
	}
	clock_t end = clock();
	double time_spent = (double)(end - begin)*1000.0 / CLOCKS_PER_SEC;
	printf("time spent: %.0f ms", time_spent);
	return 0;
}

/*
*	Related
*	https://english.stackexchange.com/questions/111765/how-to-write-out-numbers-in-compliance-with-british-usage/111837#111837
*	https://haggis.readthedocs.io/en/latest/api.html#haggis.numbers.english
*/