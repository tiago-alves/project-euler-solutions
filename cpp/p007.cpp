/**
*	21/09/2022
*
*	projecteuler problem 7 - 10001st prime
*
*	By listing the first six prime numbers: 
*	2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
*
*	What is the 10 001st prime number?
*
*/

#include <iostream>

int main()
{
	unsigned int iCurrentPrime{ 2 };

	for (unsigned short i = 1; i <= 10001; ++i)
	{
		for (unsigned int div = 2; iCurrentPrime > div; ++div)
		{
			if (!(iCurrentPrime % div))
			{
				++iCurrentPrime;
				div = 2;
			}
		}
		printf("Prime number %i: %i\n", i, iCurrentPrime);
		++iCurrentPrime;
	}
	return 0;
}