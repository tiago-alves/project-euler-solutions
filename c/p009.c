/**
*	26/09/2022
*
*	projecteuler problem 9 - Special Pythagorean triplet
*
*   A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
*
*       a2 + b2 = c2
*
*   For example, 32 + 42 = 9 + 16 = 25 = 52.
*
*   There exists exactly one Pythagorean triplet for which a + b + c = 1000.
*   Find the product abc.
*/

#include <stdio.h>

#define PYTHAGORAS(A,B) 	sqrt((A*A)+(B*B))

int main()
{
    float
        a = 1.0,
        b = 1.0,
        c = 1.0;

    while (a + b + c != 1000)
    {
        ++b;
        c = PYTHAGORAS(a, b);

        if ((a + b + c) > 1000.0)
        {
            ++a;
            b = a;
            c = 1.0;
        }
    }
    printf("found a = %.0f, b = %.0f, c = %.0f\n", a, b, c);
    printf("abc = %.0f", a * b * c);

    return 0;
}
