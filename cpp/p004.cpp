/**
*	19/09/2022
* 
*	projecteuler problem 4 - Largest palindrome product
*	
*	A palindromic number reads the same both ways. 
*	The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
*	
*	Find the largest palindrome made from the product of two 3-digit numbers.
*
*/

#include <iostream>

bool IsPalindromic(int n)
{
	if (n < 10 && n > -10) return true;


	char _szNumber[7];

	sprintf_s(_szNumber, "%i", n);

	int iNumberLength = strlen(_szNumber);

	int iHalf = iNumberLength;

	if (iHalf % 2) iHalf--;

	iHalf /= 2;

//	printf("number = %i\n", n);
//	printf("number length = %i\n", iNumberLength);
//	printf("number half length without middle digit = %i\n", iHalf);
//	printf("********************************************\n");

	for(int i = 0; i != iHalf; i++)
	{
/*		printf("digit %i = %c | mirror %i = %c\n", i,
												_szNumber[i],
												iNumberLength - i,
												_szNumber[(iNumberLength-1) - i]);
*/
		if (_szNumber[i] != _szNumber[(iNumberLength - 1) - i]) return false;
	}
	return true;
}

int main()
{
	unsigned int largestNumber{ 0 };

	for (unsigned short multiple = 999, multiplier = 999; multiple > 99; multiplier--)
	{
		static unsigned int product;
		product = multiple * multiplier;
		printf("%i * %i = %i\n", multiple, multiplier, product);

		if (IsPalindromic(product))
		{
			printf("largest palindrome %i\n", product);
			if (product > largestNumber) 
			{ 
				largestNumber = product; 
			}

			multiplier = multiple;
			--multiple;
		}
		if (multiplier == 100)
		{
			multiplier = multiple;
			--multiple;
		}
	}

	printf("largest palindrome made from the product of two 3-digit numbers is %i", largestNumber);

	return 0;
}