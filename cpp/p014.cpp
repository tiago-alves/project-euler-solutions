/**
*	30/09/2022
*
*	projecteuler problem 14 - Longest Collatz sequence
*
*	The following iterative sequence is defined for the set of positive integers:
*
*		n → n/2 (n is even)
*		n → 3n + 1 (n is odd)
*
*	Using the rule above and starting with 13, we generate the following sequence:
*	
*		13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
*	
*	It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
*	Although it has not been proved yet (Collatz Problem), 
*	it is thought that all starting numbers finish at 1.
*
*	Which starting number, under one million, produces the longest chain?
*
*	NOTE: Once the chain starts the terms are allowed to go above one million.
*/

#include <iostream>

int isEven(const int &parameter)
{
	return (!(parameter % 2));
}

int main()
{
	unsigned long long int 
							n = 0,
							iLongestChain = 0,
							iChainCount = 0;

	for (long int i = 1000000; i > 0; --i)
	{
		unsigned long long int _iNumCheck = i;
		iChainCount = 0;
		//printf("i: %i", i);
		while (_iNumCheck != 1)
		{
			if (isEven(_iNumCheck))
			{
				_iNumCheck /= 2;
			}
			else
			{
				_iNumCheck *= 3;		//	if written like ->	_iNumCheck *= 3 + 1
				_iNumCheck += 1;		//	the addition takes priority
			}
			//printf(" -> %i", _iNumCheck);
			++iChainCount;
		}
		if (iChainCount > iLongestChain)
		{
			iLongestChain = iChainCount;
			n = i;

			printf("Chain count: %llu with %llu chains\n", n, iChainCount );
		}
	}
	printf("\n");
	printf("Longest chain count: %llu with %llu chains", n, iLongestChain);
	return 0;
}
