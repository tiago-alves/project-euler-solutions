//	26/09/2021

//	project euler problem 3
//	"Largest prime factor"

/*
	The prime factors of 13195 are 5, 7, 13 and 29.

	What is the largest prime factor of the number 600851475143 ?
*/

#include <iostream>

using namespace std;

int main()
{
	long long a{ 600851475143 };
	int prime{ 2 };

	while (a != 1)
	{
		//cout << prime << endl;	

		if (!(a % prime)) 	// Chech if looping number is dividable (has 0 remainder)
		{
			cout << a << " / " << prime << " = ";	
			
			a = a / prime;	// 
			
			cout << a << endl;
		}
		else
		{
			++prime;
		}
	}
	cout << "found it: " << prime << endl;
	return 0;
}

